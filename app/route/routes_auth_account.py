from flask import Blueprint, request, jsonify, make_response
from ..database.database import db
from app.model.models import User
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
import config
from functools import wraps

mod_auth = Blueprint('auth', __name__, url_prefix='/auth')
mod_account = Blueprint('account', __name__, url_prefix='/account')


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try: 
            data = jwt.decode(token, config.SECRET_KEY)
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(current_user, *args, **kwargs)

    return decorated


@mod_auth.route('/login', methods=['POST'])
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response(jsonify({'message': 'Please enter valid login credentials'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    user = User.query.filter_by(name=auth.username).first()

    if not user:
        return make_response(jsonify({'message': 'User could not be verified'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    if check_password_hash(user.password, auth.password):
        token = jwt.encode({'public_id': user.public_id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, config.SECRET_KEY)

        return jsonify({'token': token.decode('UTF-8')})
    return make_response(jsonify({'message': 'User could not be verified'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

# Create account for user
@mod_account.route('/create', methods=['POST'])
def create_user():
    data = request.get_json()
    
    if not data["username"] and not data["password"]:
        return make_response(jsonify({'message':'Username and Password can\'t be empty'}), 401, {'WWW-Authenticate': 'Basic realm="Username and Password required!"'})
    elif not data["username"]:
        return make_response(jsonify({'message':'Username can\'t be empty'}), 401, {'WWW-Authenticate': 'Basic realm="Username required!"'})
    elif not data["password"]:
        return make_response(jsonify({'message':'Password can\'t be empty'}), 401, {'WWW-Authenticate': 'Basic realm="Password required!"'})

    hashed_password = generate_password_hash(data['password'], method='sha256')

    new_user = User(public_id=str(uuid.uuid4()), name=data['username'], password=hashed_password, admin=False)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message': 'New user created!'})

# Fetch list of users
@mod_account.route('/users', methods=['GET'])
# @token_required
# def get_all_users(current_user):
def get_all_users():
    users = User.query.all()

    output = []

    for user in users:
        user_data = {}
        user_data['id'] = user.id
        user_data['public_id'] = user.public_id
        user_data['name'] = user.name
        user_data['password'] = user.password
        user_data['admin'] = user.admin
        output.append(user_data)

    return jsonify({'users': output})

# Fetch user
@mod_account.route('/user/<public_id>', methods=['GET'])
def get_one_user(public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})

    user_data = {}
    user_data['id'] = user.id
    user_data['public_id'] = user.public_id
    user_data['name'] = user.name
    user_data['password'] = user.password
    user_data['admin'] = user.admin

    return jsonify({'user': user_data})

# Update user details
@mod_account.route('/update/user/<public_id>', methods=['POST'])
def user_update(public_id):
    data = request.get_json()
    
    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})
    
    if not data["password"]:
        return make_response(jsonify({'message':'Password can\'t be empty'}), 401, {'WWW-Authenticate': 'Basic realm="Password required!"'})

    hashed_password = generate_password_hash(data['password'], method='sha256')
    user.password = hashed_password
    # user.admin = False
    db.session.commit()
    return jsonify({'message': 'User details updated successfully!'})


# Promote user to admin details
@mod_account.route('/promte/user/<public_id>', methods=['PUT'])
def promote_user_to_admin(public_id):    
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'No user found!'})
    
    user.admin = True
    db.session.commit()
    return jsonify({'message': 'User promoted successfully!'})
