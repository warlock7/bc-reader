from flask import Blueprint, request, jsonify, make_response
import json
from ..logic.cognitiveServices import CallOperationAPI, CallVisionAPI, CallTextAnalyticsAPI
from ..logic.ProcessNER import FetchNER
from .routes_auth_account import token_required

mod_app = Blueprint('app_logic', __name__, url_prefix='/api')


# @token_required
@mod_app.route('/getner', methods=['POST'])
def GetImage():
    if not 'Content-Type' in request.headers:
        return jsonify({'message': 'Content Type is missing!'}), 401
    
    content_type_header = request.headers['Content-Type']
    if content_type_header == "application/octet-stream":
        image_data = request.data
        operation_location_api = CallVisionAPI(image_data)
        if operation_location_api.__contains__('Exception'):
            # response_message = operation_location_api
            # abort(400, operation_location_api)
            return jsonify({'message': operation_location_api}), 401
        else:
            operation_location_api_result = CallOperationAPI(operation_location_api)
            if operation_location_api_result.__contains__('Exception'):
                # abort(400, operation_location_api_result)
                return jsonify({'message': operation_location_api_result}), 401
            else:
                text_analytics_api_result = CallTextAnalyticsAPI(operation_location_api_result)
                if text_analytics_api_result.__contains__('Exception'):
                    # abort(400, text_analytics_api_result)
                    return jsonify({'message': text_analytics_api_result}), 401
                else:
                    ner_result = FetchNER(text_analytics_api_result)
                    if ner_result.__contains__('Exception'):
                        # abort(400, ner_result)
                        return jsonify({'message': ner_result}), 401
                    else:
                        result = {**ner_result, **operation_location_api_result}
                        result = json.dumps(result)
                        # operation_location_api_result = json.dumps(operation_location_api_result)
                        # resp = str(ner_result) + str(operation_location_api_result)
                        
                        return str(result)
    else:
        # abort(400, "Content Type is not matching")
        return jsonify({'message': 'Content Type is not matching'}), 401
