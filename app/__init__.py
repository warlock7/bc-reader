from flask import Flask
from app.route.routes_auth_account import mod_auth as mod_auth
from app.route.routes_auth_account import mod_account as mod_account
from app.route.routes_app_logic import mod_app as mod_app
from app.database.database import db
from app.model.models import User

app = Flask(__name__)

app.config.from_object('config')

app.app_context().push()
db.init_app(app)


app.register_blueprint(mod_auth)
app.register_blueprint(mod_account)
app.register_blueprint(mod_app)
