import requests
import json
from time import sleep

# Calls Operation-Location-API and get Text in JSON format


def CallOperationAPI(operation_Location):
    try:
        url = operation_Location
        head = {'Ocp-Apim-Subscription-Key': '182e9d574eae4f32ae1c592c4aa06d9f'}
        while True:
            sleep(5)
            api_result = requests.get(url, headers=head, timeout=5)
            api_result_json = api_result.json()
            if api_result.status_code == 200:
                if api_result_json['status'].__contains__('Succeeded'):
                    count = 1
                    json_list = []
                    json_dictionary = {}
                    for fetch_text in api_result_json['recognitionResult']['lines']:
                        text = fetch_text['text']
                        language = 'en'
                        id = str(count)
                        json_list.append(
                            {'id': id, 'language': language, 'text': text})
                        count = count + 1
                    json_dictionary['documents'] = json_list
                    call_operation_api_result = json_dictionary
                    break
                call_operation_api_result = "Loading... Please wait!"
                sleep(5)
            else:
                call_operation_api_result = 'Exception in call_Operation_API: ' + \
                    str(api_result.status_code) + ' ' + \
                    str(api_result_json['error']['message'])
                break
        return call_operation_api_result
    except Exception as ExceptionMessage:
        exception = 'Exception in call_Operation_API: ' + str(ExceptionMessage)
        return exception


# Calls Azure-Vision-API and get Operation-Location-API
def CallVisionAPI(image_data):
    try:
        url = 'https://centralus.api.cognitive.microsoft.com/vision/v2.0/recognizeText?mode=Printed'
        head = {'Content-type': 'application/octet-stream',
                'Ocp-Apim-Subscription-Key': '182e9d574eae4f32ae1c592c4aa06d9f'}

        api_result = requests.post(
            url, headers=head, data=image_data, timeout=5)
        if api_result.status_code == 202:
            call_vision_api_result = api_result.headers['Operation-Location']
        else:
            api_result_json = api_result.json()
            call_vision_api_result = 'Exception in call_Vision_API: ' + \
                str(api_result.status_code) + ' ' + \
                str(api_result_json['error']['message'])
        return call_vision_api_result
    except Exception as ExceptionMessage:
        exception = 'Exception in call_Vision_API: ' + str(ExceptionMessage)
        return exception


# Calls Azure-Text-Analytics-API and get the NER of the requested text
def CallTextAnalyticsAPI(json_request):
    try:
        params = json.dumps(json_request)
        url = 'https://centralus.api.cognitive.microsoft.com/text/analytics/v2.1-preview/entities'
        head = {'Content-type': 'application/json',
                'Ocp-Apim-Subscription-Key': '4560d3ebffe6482eaf8ca38ed00cf5f7'}
        api_result = requests.post(url, headers=head, data=params, timeout=5)
        if api_result.status_code == 200:
            call_text_analytics_api_result = api_result.text
        else:
            api_result_json = api_result.json()
            call_text_analytics_api_result = 'Exception in call_Text_Analytics_API: ' + \
                str(api_result_json['message']) + ' ' + \
                str(api_result_json['innerError']['message'])
        return call_text_analytics_api_result
    except Exception as ExceptionMessage:
        exception = 'Exception in call_Text_Analytics_API: ' + \
            str(ExceptionMessage)
        return exception
