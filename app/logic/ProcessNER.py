import json
import re


def FetchNER(request):
    try:
        request_json = json.loads(request)
        fetch_ner_response_list = []
        fetch_ner_response_dict = {}
        for n in request_json["documents"]:
            ner = []
            text = []
            text1 = []
            loc = ''
            ner_loc = ''
            ner_type_loc = ''
            count_quantity = 0
            for i in n["entities"]:
                if i["type"] == "Email":
                    for e in i["matches"]:
                        text = e["text"]
                        ner_type = i["type"]
                elif i["type"] == "Organization":
                    for e in i["matches"]:
                        text = e["text"]
                        ner_type = i["type"]
                elif i["type"] == "URL":
                    for e in i["matches"]:
                        text = e["text"]
                        ner_type = i["type"]             
                elif i["type"] == "Person":
                    for e in i["matches"]:
                        text = e["text"]
                        ner_type = i["type"] 
                elif i["type"] == "Quantity":
                    if i["subType"] == "Number":
                        for e in i["matches"]:
                            if e["offset"] < 16:
                                text.append(e["text"])
                                ner_type = i["type"]
                            else:
                                text1.append(e["text"])
                                ner_type = i["type"]
                elif i["type"] == "Location":
                    for e in i["matches"]:
                        loc = e["text"]
                        ner_type_loc = i["type"]
                elif i["type"] == "Other":
                    for e in i["matches"]:
                        if any(i.isdigit() for i in e["text"]):
                            text = re.findall(r'\d+', e["text"])
                            ner_type = i["type"]
                elif count_quantity > 1:
                    if i["type"] == "DateTime":
                        for e in i["matches"]:
                            text.append(e["text"])
                            ner_type = "Quantity"
                        
            if loc and ner_type_loc:
                if any(fetch_ner_response_list_item["type"] == "Location" for fetch_ner_response_list_item in fetch_ner_response_list):
                    for fetch_ner_response_list_item in fetch_ner_response_list:
                        if fetch_ner_response_list_item["type"] in "Location":
                            loc = fetch_ner_response_list_item["text"] + ', ' + loc
                            fetch_ner_response_list.append({"text" : loc, "type" : ner_type_loc})
                            fetch_ner_response_list.remove(fetch_ner_response_list_item)
                            break
                else:
                    fetch_ner_response_list.append({"text" : loc, "type" : ner_type_loc})
            elif text and ner_type:
                if ner_type == "Quantity":
                    if text and text1:
                        text = "-".join(text)
                        text1 = "-".join(text1)
                        fetch_ner_response_list.append({"text" : text, "type" : ner_type})
                        fetch_ner_response_list.append({"text" : text1, "type" : ner_type})
                    elif text:
                        text = "-".join(text)
                        fetch_ner_response_list.append({"text" : text, "type" : ner_type})
                    elif text1:
                        text1 = "-".join(text1)
                        fetch_ner_response_list.append({"text" : text1, "type" : ner_type})
                else:
                    fetch_ner_response_list.append({"text" : text, "type" : ner_type})
        fetch_ner_response_dict["ner"] = fetch_ner_response_list
        return(fetch_ner_response_dict)        
    except Exception as ExceptionMessage:
        exception = 'Exception in FetchNER: ' + str(ExceptionMessage)
        return exception  
